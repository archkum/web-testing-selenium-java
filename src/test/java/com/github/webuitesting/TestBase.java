package com.github.webuitesting;

import com.github.webuitesting.config.TestConfiguration;
import com.github.webuitesting.util.Browser;
import com.github.webuitesting.webdriver.WebDriverFactory;
import com.relevantcodes.extentreports.ExtentReports;
import org.openqa.selenium.WebDriver;
import org.seleniumhq.selenium.fluent.FluentWebDriver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;

import javax.inject.Inject;
import java.util.concurrent.TimeUnit;

/*
 * Base class for all the test classes
 * 
 */
@ContextConfiguration( classes = { TestConfiguration.class} )
public class TestBase extends AbstractTestNGSpringContextTests {
	private static final String SCREENSHOT_FOLDER = "target/screenshots/";
	private static final String SCREENSHOT_FORMAT = ".png";
    static final String extentReportLocation = "target/extentreports/";


	@Value("${grid2.hub}")
	protected String gridHubUrl;
	@Value("${page.url}")
	protected String websiteUrl;
	@Value("${browser.name}")
	protected String browserName;
	@Value("${browser.version}")
	protected String browserVersion;
	@Value("${browser.platform}")
	protected String browserPlatform;
	@Value("${user.username}")
	protected String userName;
	@Value("${user.password}")
	protected String password;

    protected WebDriver webDriver;

    protected final ExtentReports extent = new ExtentReports (extentReportLocation + this.getClass().getName() + ".html");

	@BeforeClass
	public void init() {
        Browser browser = new Browser();
		browser.setName(browserName);
		browser.setVersion(browserVersion);
		browser.setPlatform(browserPlatform);

		webDriver = WebDriverFactory.getInstance(gridHubUrl, browser, userName,password);
		webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
        extent.flush();
        extent.close();
		if (webDriver != null) {
			webDriver.quit();
		}
	}

//	@AfterMethod
//	public void setScreenshot(ITestResult result) {
//		if (!result.isSuccess()) {
//			try {
//				WebDriver returned = new Augmenter().augment(webDriver);
//				if (returned != null) {
//					File f = ((TakesScreenshot) returned)
//							.getScreenshotAs(OutputType.FILE);
//					try {
//						FileUtils.copyFile(f, new File(SCREENSHOT_FOLDER
//								+ result.getName() + SCREENSHOT_FORMAT));
//					} catch (IOException e) {
//						e.printStackTrace();
//					}
//				}
//			} catch (ScreenshotException se) {
//				se.printStackTrace();
//			}
//		}
//	}
}
