package com.github.webuitesting.webdriverguide;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.MarionetteDriver;

/**
 * Created by archana on 1/23/15.
 */
public class GoogleSearch {
    public static void main(String[] args){
        WebDriver driver = new ChromeDriver();
        driver.get("http://www.google.com");
        WebElement searchBox = driver.findElement(By.name("q"));
        searchBox.sendKeys("Packt Publishing");
        searchBox.submit();
        driver.close();
    }
}